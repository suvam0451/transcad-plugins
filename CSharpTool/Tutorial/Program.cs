﻿using System;
using System.Text;
using System.Threading.Tasks;
using CaliperForm;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Tutorial
{
    class Program
    {
        static string log_file = "C:\\Users\\asksy\\Desktop\\Debashish\\VS Projects\\Tutorial\\log.txt";
        static string mapping_product = "TransCAD";

        static void Main(string[] args)
        {
            // Open_Map();
            // Open_Table();
            // Tutorial_Table();
            MapQueryExample();
            // CaliperForm
        }

        static void Open_Map() {
            CaliperForm.Connection Conn = new CaliperForm.Connection { MappingServer = mapping_product };
            Boolean opened = false;
            try
            {
                opened = Conn.Open(log_file);
                if (opened)
                {
                    dynamic dk = Conn.Gisdk;

                    // string tutorial_folder = dk.Macro("G30 Tutorial Folder") as string;
                    string tutorial_folder = "C:\\Users\\asksy\\Desktop\\Debashish\\" as string;

                    // strig tuto
                    Object[] program_info = dk.GetProgram();

                    string mypath = "C:\\Users\\asksy\\Desktop\\Assignment\\TransCAD\\Mandl Network";


                    // string map_file = tutorial_folder + "BestRout.map";
                    string map_file = tutorial_folder + "tutorial.map"; // modified...
                    Console.WriteLine(map_file);
                    // Console.ReadLine();

                    dk.SetSearchPath(mypath);


                    var map_options = new OptionsArray();
                    map_options["Auto Project"] = "true";

                    // Seeking layers in these folders...
                    map_options["Force Directory"] = System.IO.Path.GetDirectoryName(map_file);
                    
                    // We set directory and filename...
                    string map_name = dk.OpenMap(map_file, map_options) as string;

                    // null checks...
                    if (map_name == null)
                    {
                        Console.Out.WriteLine("Cannot open map " + map_file + ". Perhaps some layers cannot be found?");
                        return;
                    }
                    else
                    {
                        Console.Out.WriteLine("map_name = " + map_name);
                    }

                    // Set the current layer
                    // dk.SetLayer("County");
                    dynamic layers = dk.GetMapLayers(map_name, "All");

                    // API to get data about workspace...
                    dynamic layer_names = layers[0];
                    int current_idx = (int)layers[1];
                    string current_layer = layers[2] as string;

                    Console.WriteLine(current_layer);
                    Console.ReadLine();
                    // string map_file = tutorial_folder + "BestRout.map";

                    // Closing instance...
                    dk.CloseMap(map_name);
                    Conn.Close();
                }
            }
            catch {
                Console.Write("Running instace not found...");
                // throw err
            }
        }

        static void Open_Table() {
            CaliperForm.Connection Conn = new CaliperForm.Connection { MappingServer = mapping_product };
            Boolean opened = false;
            try
            {
                opened = Conn.Open(log_file);
                if (opened)
                {
                    dynamic dk = Conn.Gisdk;
                    string tutorial_folder = dk.Macro("G30 Tutorial Folder") as string;
                    // sales is a .bin file
                    string table_name = dk.OpenTable("sales", "ffb", new Object[] { tutorial_folder + "ctsales.bin", null });

                    // First: names, second: specs...
                    object[] fields = dk.GetFields(table_name, "All");
                    var field_names = fields[0] as object[]; // Array of table header
                    var field_specs = fields[1] as object[]; // In format tablename.tablekey e.g. - sales.Key

                    dk.SetView(table_name);
                    int num_rows = dk.GetRecordCount(table_name, null); // Gets number of rows
                    Console.WriteLine(num_rows);

                    // Debug
                    // Console.WriteLine(string.Join(",", field_specs));
                    // Console.WriteLine(string.Join(",", field_names));

                    // Example query
                    string query = "select * where Population > 200000";
                    int num_found = dk.SelectByQuery("large towns", "several",
                        query, new Object[] { new Object[] { "Index Limit", 0 } });

                    Console.WriteLine(num_found);
                    if (num_found > 0)
                    {
                        string view_set = table_name + "|large towns"; // a|xyz
                        object[] sort_order = null;
                        object[] options = null;
                        string order = "Row";
                        string first_record = dk.GetFirstRecord(view_set, null);


                        // PRints a query result
                        object[] myquery = dk.GetRecordsValues(view_set, first_record, field_names,
                            sort_order, num_found, order, null);

                        foreach (object[] row in myquery)
                        {
                            string row_values = string.Join(",", row);
                            Console.Out.WriteLine(row_values);
                        }
                        Console.WriteLine(query);
                        Console.WriteLine(string.Join(",", field_specs));
                    }
                    dk.CloseView(table_name);
                    Console.WriteLine();
                    Console.ReadLine();
                    Conn.Close();
                }
            }
            catch {
                Console.WriteLine("Lcense expired ?");
            }
        }

        static void Tutorial_Table()
        {
            CaliperForm.Connection Conn = new CaliperForm.Connection { MappingServer = mapping_product };
            Boolean opened = false;
            try
            {
                opened = Conn.Open(log_file);
                if (opened) {
                    dynamic dk = Conn.Gisdk;
                    
                    // string tutorial_folder = dk.Macro("G30 Tutorial Folder") as string;
                    // string tutorial_folder = "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\" as string;
                    string tutorial_folder = "C:\\Users\\asksy\\Desktop\\Debashish\\WCTRS\\Links\\" as string;

                    // string table_name = dk.OpenTable("Trial_Harsh", "ffb", new Object[] { tutorial_folder + "Mandl_links_full.bin", null });
                    string table_name = dk.OpenTable("Trial_Harsh", "ffb", new Object[] { tutorial_folder + "Links_June_2018_R2.bin", null });

                    Console.WriteLine("So far, so good...");
                    // First: names, second: specs...
                    object[] fields = dk.GetFields(table_name, "All");
                    var field_names = fields[0] as object[]; // Array of table header
                    var field_specs = fields[1] as object[]; // In format tablename.tablekey e.g. - sales.Key

                    dk.SetView(table_name);
                    int num_rows = dk.GetRecordCount(table_name, null); // Gets number of rows
                    Console.WriteLine(num_rows);

                    // Debug
                    // Console.WriteLine(string.Join(",", field_specs));
                    // Console.WriteLine(string.Join(",", field_names));

                    // Example query (This works)...
                    string query = "select * where [Travel Time] > 2";
                    int num_found = dk.SelectByQuery("large towns", "several",
                        query, new Object[] { new Object[] { "Index Limit", 0 } });

                    Console.WriteLine(num_found);
                    if (num_found > 0) {

                        // To b shhifted to a macro function...
                        string view_set = table_name + "|large towns"; // a|xyz
                        object[] sort_order = null;
                        object[] options = null;
                        string order = "Row";
                        string first_record = dk.GetFirstRecord(view_set, null);

                        // PRints a query result (Heart of the query...)
                        object[] myquery = dk.GetRecordsValues(view_set, first_record, field_names,
                            sort_order, num_found, order, null);

                        TextWriter tw = new StreamWriter("Output.csv");
                        tw.WriteLine(string.Join(",", field_names));
                        foreach (object[] row in myquery)
                        {
                            string row_values = string.Join(",", row);
                            Console.Out.WriteLine(row_values);
                            tw.WriteLine(row_values);
                        }
                        tw.Close();
                    }
                    // sales is a .bin file
                    // string table_name = dk.OpenTable("sales", "ffb", new Object[] { tutorial_folder + "ctsales.bin", null });
                }
                Console.ReadLine();
            }
            catch {
            }

        }

        static void MapQueryExample() {
            CaliperForm.Connection Conn = new CaliperForm.Connection { MappingServer = mapping_product };
            Boolean opened = false;
            try
            {
                opened = Conn.Open(log_file);
                if (opened)
                {
                    dynamic dk = Conn.Gisdk;

                    // Locations
                    // string tutorial_folder = dk.Macro("G30 Tutorial Folder") as string;
                    string path = "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network";
                    string tutorial_folder = "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\" as string;
                    // string tutorial_folder = "C:\\Users\\asksy\\Desktop\\Debashish\\WCTRS\\Links\\" as string;

                    // Paths
                    string netfile_location = tutorial_folder + "MyNewMap.map";
                    // string netfile_location = tutorial_folder + "Ekm.map";

                    var map_options = new OptionsArray();
                    map_options["Auto Project"] = true;
                    map_options["Force Directory"] = tutorial_folder;

                    string map_name = dk.OpenMap(netfile_location, map_options) as string;
                    object[] layer_array = dk.GetLayers();

                    string map_filename = dk.GetMapNetworkFileName(map_name);
                    Console.WriteLine(map_filename);

                    // Gives {Array, currentIndex, currentName}
                    object[] layer_list = layer_array[0] as object[];
                    // Console.WriteLine(layer_array[0]);
                    // Console.WriteLine(layer_array[1]);
                    // Console.WriteLine(layer_array[2]);

                    string targetlayer = "";
                    foreach (string layer in layer_list) {
                        Console.WriteLine(layer);
                    }

                    // --------------------------------------------------------------
                    // ------ Adding route system -----------------------------------
                    var rts_options = new OptionsArray();
                    // rts_options["Auto Project"] = true;
                    // rts_options["Force Directory"] = tutorial_folder;


                    string rts_path = Path.Combine(path, "final routes", "Routes.rts");
                    // object[] lyrs = dk.AddRouteSystemLayer(map_name, layer_list[1], rts_path, rts_options);
                    // dk.Macro("Set Default RS Style", lyrs, "True", "True");
                    // dk.SaveMap(map_name, "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\out\\testMap.map");
                    // --------------------------------------------------------------


                    // --------------------------------------------------------------
                    //              Route system from table


                    // string table_file = Path.Combine(path, "Routes_table_new2.DCB");
                    string table_file = Path.Combine(path, "new-routes-bin.bin");
                    string tour_table_file = Path.Combine(path, "Routes_table_new.bin");


                    string geo_file = Path.Combine(path, "Mandl_links_full.dbd");
                    string new_route_file = Path.Combine(path, "out", "NewRoute.rts");

                    // Try to run via macro...
                    var opt = new OptionsArray();
                    // opt["Input"] = new object[3];
                    // opt["Global"] = new object[10];
                    // opt["Output"] = new object[1];
                    // opt["Network"] = Path.Combine(path, "Mandl_network.net");
                    opt["Input"]["Network"] = Path.Combine(path, "Mandl_network.net");
                    // opt["Link Set"] = "Mandl Lines_C1";
                    opt["Input"]["Link Set"] = "Mandl_links_C2.dbd|Mandl lines_C";
                    opt["Input"]["Tour Table"] = tour_table_file;

                    opt["Global"]["Cost Field"] = 1;
                    opt["Global"]["Auto Project"] = true;
                    opt["Global"]["Force Directory"] = tutorial_folder;
                    opt["Global"]["Route ID Field"] = 1;
                    opt["Global"]["Node ID Field"] = 2;
                    opt["Global"]["Include Stop"] = 0;
                    opt["Global"]["RS Layers"]["RouteLayer"] = "Route System";
                    opt["Global"]["RS Layers"]["StopLayer"] = "Route Stops";
                    opt["Global"]["Stop Flag Field"] = 3;
                    opt["Global"]["User ID Field"] = 4;
                    opt["Output"]["Output Routes"] = Path.Combine(path, "out", "out.rts");


                    // string rs = dk.Macro("TCB Run Operation", "Create RS From Table", opt) as string;
                    string str = dk.Macro("TCB Run Operation", "Create RS From Table", opt) as string;

                    var rs_opt = new OptionsArray();
                    rs_opt["Route Number"] = 1;
                    // string info = dk.CreateRouteSystemFromTables(table_file, geo_file,
                    //     "Mandl Lines", new_route_file, rs_opt);


                    // string table_path = Path.Combine(path, )
                    // dk.CreateRouteSystemFromTables()
                    // --------------------------------------------------------------


                    // dk.SetLayer("County");
                    // object[] LinkArray = dk.GetNetworkNodeLinks(map_name, 43);
                    // dk.ShowArray(LinkArray);
                    // 

                    //Create Formula Field
                    Opts = null;
                    Opts.Input.[View Set] = { path + "TOPRINT.BIN", "TOPRINT"};
                    Opts.Global.[Field Name] = "Change in Sales";
                    Opts.Global.[Formula Text] = "Sales - [Sales Last Year]";
                    Global.[Field Type] = "Integer";
                    ret_value = RunMacro("TCB Run Operation", "Formula Field", Opts);


                    dk.SaveMap(map_name, "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\out\\testMap.map");
                    Console.WriteLine("Done");
                    Conn.Close();
                    Console.ReadLine();
                }




                // dk.Op
                // string tutorial_folder = "C:\\Users\\asksy\\Desktop\\Debashish\\WCTRS\\Links\\" as string;
                // Network net1 ReadNetwork()

                // Valid for maps
                // string map_name = dk.OpenMap(netfile_location, map_options);

                // dk.ReadNetwork(netfile_location, map_options);

                // var n_nodes = dk.Network(net_handle);

                // ReadNetwork()
            }
            catch(System.Data.DataException) {
                Console.WriteLine("Something went wrong...");
                Console.ReadLine();
            }
            // shared ActiveNetwork;   
        }
    }
}

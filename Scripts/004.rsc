
// Geneartes RouteSystem from tour table
macro "RSFromTable"
    // path variables
    path = "C:\\Users\\asksy\\Desktop\\Debashish\\Workspace_MN\\"
    net_file = "Mandl_Net_WS_1.net"
    dbd_file = "Mandl_Links_WS_1.dbd"
    tour_file = "Routes_TF_WS_1.bin" // use the bin file

    line_layer = "Mandl Lines_WS_1s"
    // ---------------

    Opts = null
    Opts.Input.Network = path + net_file
    Opts.Input.[Link Set] = { path + net_file + "|" + line_layer, "Okay"}
    Opts.Input.[Tour Table] = { path + tour_file }
    // --- Table mappings
    Opts.Global.[Cost Field] = 1
    Opts.Global.[Route ID Field] = 1
    Opts.Global.[Node ID Field] = 2
    Opts.Global.[Include Stop] = 1
    Opts.Global.[Stop Flag Field] = 3
    Opts.Global.[User ID Field] = 4
    // -------------------
    Opts.Output.[Output Routes] = path + "out\\test.rts"

    // Process --------------
    RunMacro("TCB Init")
    str = RunMacro("TCB Run Operation", "Create RS From Table", Opts)
endmacro

/*
Macro "another macro"
    path = "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\"
    Opts = null
    Opts.Input.Network = path + "Mandl_network.net"
    Opts.Input.[Link Set] = { path + "Mandl_links_C2.dbd|Mandl Lines_C1", "Okay"}
    Opts.Input.[Tour Table] = { path + "Routes_table_new2.bin" }
    Opts.Global.[Cost Field] = 1
    Opts.Global.[Route ID Field] = 1
    Opts.Global.[Node ID Field] = 2
    Opts.Global.[Include Stop] = 1
    Opts.Global.[Stop Flag Field] = 3
    Opts.Global.[User ID Field] = 4
    Opts.Output.[Output Routes] = path + "out\\test.rts"
    RunMacro("TCB Init")
    str = RunMacro("TCB Run Operation", "Create RS From Table", Opts)
endMacro */


// Generates TransitNetwork for given Route(.rts) file
Macro "MinorInconvenience"
    // Path declaration
    path = "C:\\Users\\asksy\\Desktop\\Debashish\\Workspace_MN\\" // Don't forget \\ at end. Also \\ for paths
    route_file = "out\\test.rts" // .rts
    map_file = "Mandl_Map_WS_R1.map" // .map
    // layers
    routes_layer = "Vehicle Routes"
    stops_layer = "Stops"

    Opts = null
    Opts.Input.[Transit RS] = path + route_file
    Opts.Input.[RS Set] = {path + route_file + "|" + routes_layer, routes_layer}
    Opts.Input.[Stop Set] = {path + map_file + "|" + stops_layer, stops_layer}

    // --------------------- COPY-PASTE section --------------------------------------------------
    Opts.Field.[Link Fields] = {"Distance", "Dir", "Length", "Cost", "[Bus Time]", "[Auto Time]", "[Bus Speed]", "[Auto Speed]"}
    Opts.Field.[Route Fields] = {"Route_Number", "Time", "Distance"}
    Opts.Field.[Stop Fields] = {"Pass_Count", "Milepost", "STOP_ID", "UserID", "Route_Number", "Node_ID", "Stop_Flag", "Stop_Name", "Cap"}
    Opts.Field.[Node Fields] = {"Elevation", "[ID:1]", "check"}
    Opts.Field.[Tag Method] = 1
    Opts.Field.[Tag Field] = "UserID"
    Opts.Global.[Network Label] = "Based on 'Vehicle Routes' (Thu Mar 05 15:50:54 2020)"
    Opts.Global.[Network Options].Walk = "No"
    Opts.Global.[Network Options].[Link Attributes] = {{"Distance", {"[Mandl Lines_WS_1].Distance", "[Mandl Lines_WS_1].Distance"}, "SUMFRAC"}, {"Dir", {"[Mandl Lines_WS_1].Dir", "[Mandl Lines_WS_1].Dir"}, "SUMFRAC"}, {"Length", {"[Mandl Lines_WS_1].Length", "[Mandl Lines_WS_1].Length"}, "SUMFRAC"}, {"Cost", {"[Mandl Lines_WS_1].Cost", "[Mandl Lines_WS_1].Cost"}, "SUMFRAC"}, {"[Bus Time]", {"[Mandl Lines_WS_1].[Bus Time]", "[Mandl Lines_WS_1].[Bus Time]"}, "SUMFRAC"}, {"[Auto Time]", {"[Mandl Lines_WS_1].[Auto Time]", "[Mandl Lines_WS_1].[Auto Time]"}, "SUMFRAC"}, {"[Bus Speed]", {"[Mandl Lines_WS_1].[Bus Speed]", "[Mandl Lines_WS_1].[Bus Speed]"}, "SUMFRAC"}, {"[Auto Speed]", {"[Mandl Lines_WS_1].[Auto Speed]", "[Mandl Lines_WS_1].[Auto Speed]"}, "SUMFRAC"}}
    Opts.Global.[Network Options].[Route Attributes].Route_Number = {"[Vehicle Routes].Route_Number"}
    Opts.Global.[Network Options].[Route Attributes].Time = {"[Vehicle Routes].Time"}
    Opts.Global.[Network Options].[Route Attributes].Distance = {"[Vehicle Routes].Distance"}
    Opts.Global.[Network Options].[Stop Attributes].Pass_Count = {"Stops.Pass_Count"}
    Opts.Global.[Network Options].[Stop Attributes].Milepost = {"Stops.Milepost"}
    Opts.Global.[Network Options].[Stop Attributes].STOP_ID = {"Stops.STOP_ID"}
    Opts.Global.[Network Options].[Stop Attributes].UserID = {"Stops.UserID"}
    Opts.Global.[Network Options].[Stop Attributes].Route_Number = {"Stops.Route_Number"}
    Opts.Global.[Network Options].[Stop Attributes].Node_ID = {"Stops.Node_ID"}
    Opts.Global.[Network Options].[Stop Attributes].Stop_Flag = {"Stops.Stop_Flag"}
    Opts.Global.[Network Options].[Stop Attributes].Stop_Name = {"Stops.Stop_Name"}
    Opts.Global.[Network Options].[Stop Attributes].Cap = {"Stops.Cap"}
    Opts.Global.[Network Options].[Street Node Attributes] = {{"Elevation", {"Endpoints_WS_1.Elevation"}}, {"[ID:1]", {"Endpoints_WS_1.[ID:1]"}}, {"check", {"Endpoints_WS_1.check"}}}
    Opts.Global.[Network Options].TagField = "UserID"
    Opts.Global.[Network Options].[Merge Stops] = {"Stops.ID", "Stops.UserID"}
    // ------------------------------------------------------------------------------------
    Opts.Output.[Network File] = path + "out\\TransitNetwork.tnw" // Relative output path

    // Process -----------------------------
    RunMacro("TCB Init")
    ret_value = RunMacro("TCB Run Operation", "Build Transit Network", Opts, &Ret)
EndMacro


// This section has the default list of params
Macro "FullFieldList"
    //  Transit Network Setting PF

    // ---------------------------------------------
    //      INPUTS (we will handle later)
    // ---------------------------------------------

    Opts = null
    Opts.Input.[Transit Network] = path + "parkandride.tnw"
    Opts.Input.[Transit RS] = path + "transit.rts"
    Opts.Input.[Centroid Set] = {path + "SP_STR.CDF|Node", "Node","Centroids", "Select * where centroid = 1"}
    Opts.Input.[Parking Node Set] = {path + "SP_STR.CDF|Node", "Node","Park and Ride", "Select * where [Park and Ride Node] = 1"}
    // Opts.Input.[Egress Parking Node Set] // The set of nodes chosen from the node layer to be designated as egress parking nodes
    // Opts.Input.[Driving Link Set]
    Opts.Input.[Mode Table] = {path + "ModeTable.bin"}
    Opts.Input.[Mode Cost Table] = {path + "ModeTransferTable.bin"}
    Opts.Input.[Xfer Wait Table] 
    Opts.Input.[Xfer Fare Table]
    Opts.Input.[Acce Park Table]
    Opts.Input.[Egre Park Table]
    Opts.Input.[Fare Currency]
    Opts.Input.[OP Time Currency]
    Opts.Input.[OP Dist Currency]
    Opts.Input.[OP Cost Currency]
    Opts.Input.[Acce Park Currency]
    Opts.Input.[Egre Park Currency]


    // ---------------------------------------------
    //                  GLOBALS
    // ---------------------------------------------

    // Opts.Global.[Global Fare Type] =         // String(Number)            // "Flat", "Zonal" or "Stop"
    Opts.Global.[Global Fare Value] = 1         // Double                    // The default base flat fare
    Opts.Global.[Global Xfer Fare] = 0.4        // Double                    // The default flat transfer fare
    // Opts.Global.[Global Fare Core] =         // Integer                   // The default (0-based) position of the zonal fare matrix core
    Opts.Global.[Global Fare Weight] = 1        // Double                    // The default fare weight value, >=0
    Opts.Global.[Global Imp Weight] = 1         // Double                    // The default link impedance/time weight, >0                
    Opts.Global.[Global Init Weight] = 1        // Double                    // The default initial boarding penalty time weight, >=0
    Opts.Global.[Global Xfer Weight] = 1        // Double                    // The default transfer boarding penalty time weight, >=0
    Opts.Global.[Global IWait Weight] = 2       // Double                    // The default initial waiting time weight, >=0
    Opts.Global.[Global XWait Weight] = 2       // Double                    // The default transfer waiting time weight, >=0
    Opts.Global.[Global Dwell Weight] = 1       // Double                    // The default dwelling time weight, >=0s
    Opts.Global.[Global Headway] = 15           // Double                    // The default headway, >0
    
    // Time setings
    Opts.Global.[Global Init Time]              // Double                   // The default initial boarding penalty time, >=0
    Opts.Global.[Global Xfer Time]              // Double                   // The default transfer boarding penalty time, >=0
    Opts.Global.[Global Max IWait]              // Double                   // The default maximum initial waiting time
    Opts.Global.[Global Min IWait]              // Double                   // The default minimum initial waiting time
    Opts.Global.[Global Max XWait]              // Double                   // The default maximum transfer waiting time
    Opts.Global.[Global Min XWait]              // Double                   // The default minimum transfer waiting time
    Opts.Global.[Global Dwell On Time]          // Double                   // The default dwelling-on time value, >=0
    Opts.Global.[Global Dwell Off Time]         // Double                   // The default dwelling-off time value, >=0
    Opts.Global.[Global Layover Time]           // Double                   // The default layover time
    Opts.Global.[Global Max WACC Path]          // Integer                  // The default maximum number of zonal walk accesses
    Opts.Global.[Global Max PACC]               // Double                   // The default maximum parking-to-station walk times
    Opts.Global.[Global Max Access]             // Double                   // The default maximum walk access time
    Opts.Global.[Global Max Egress]             // Double                   // The default maximum walk egress time
    Opts.Global.[Global Max Transfer]           // Double                   // The default maximum walk transfer time
    Opts.Global.[Global Max Imp]                // Double                   // The default maximum modal total link impedance/time
    Opts.Global.[Value of Time]                 // Double                   // The default value of time ($/minute, > 0)
    Opts.Global.[Max Xfer Number]               // Integer                  // The default maximum number of transfers allowed, >=0
    Opts.Global.[Max Trip Time]                 // Double                   // The default maximum total travel cost, >=0
    Opts.Global.[Max Xfer Time]                 // Double                   // The default maximum walk transfer time
    Opts.Global.[Max Acce Drive Time]           // Double                   // The default maximum access driving time, > 0
    Opts.Global.[Max Egre Drive Time]           // Double                   // The default maximum egress driving time, > 0
    Opts.Global.[Max Acce Drive Paths]          // Double                   // The maximum number of drive access paths, > 0
    Opts.Global.[PNR Alpha]                     // Double                   // The parking capacity BPR Alpha parameter
    Opts.Global.[PNR Beta]                      // Double                   // The parking capacity BPR Beta parameter
    Opts.Global.[PNR Capacity]                  // Double                   // The parking capacity
    Opts.Global.[Walk Weight]                   // Double                   // The default walking time weight, >=0
    Opts.Global.[Drive Time Weight]             // Double                   // The default driving time weight, >=0
    Opts.Global.[interarrival Para]             // Double                   // The default inter-arrival parameter, 0-1
    Opts.Global.[Logit Scale Para]              // Double                   // The default scale parameter of Logit choice model
    Opts.Global.[Path Threshold]                // Double                   // The threshold value for combining transit paths, -1, 0-1, with -1 meaning no path combination
    Opts.Global.[Walk Path Threshold]           // Double                   // The threshold value for combining walk paths, 0-1
    Opts.Global.[Drive Path Threshold]          // Double                   // The threshold value for combining drive access paths, 0-1
    Opts.Global.[Mid-block Threshold]           // Double                   // The threshold value for ignoring mid-block stop offset, 0-1
    Opts.Global.[Zonal Fare Method]             // String(Number)            // The zonal fare charge method: "By Route", "By Mode" or "By OD"


    // ---------------------------------------------
    //                  FIELDS   (All strings)
    // ---------------------------------------------

    Opts.Field.[Node Max WACC Path] = "IVTT"            // The name of the field in the **node table** that contains the maximum number of walk access paths
    Opts.Field.[Max Acce Drive Time]                    // The name... access driving time
    Opts.Field.[Max Egre Drive Time]                    // The name... maximum egress driving time
    Opts.Field.[Max Acce Drive Paths]                   // The name... maximum number of drive access paths
    Opts.Field.[PNR Alpha]                              // The name... parking capacity BPR Alpha parameter value
    Opts.Field.[PNR Beta]                               // The name... parking capacity BPR Beta parameter value
    Opts.Field.[PNR Capacity]                           // The name... parking capacity
    Opts.Field.[Node Walk Threshold]                    // The name... threshold value for combining walk paths
    Opts.Field.[Node Drive Threshold]                   // The name... threshold value for combining drive access paths
    Opts.Field.[Link Impedance]                         // The name of the field in the **link table** that contains the impedance/time value
    Opts.Field.[Link Drive Time]                        // The name... driving times
    Opts.Field.[Stop Zone ID]                           // The name of the field in the **stop table** that contains the fare zone ID
    Opts.Field.[Stop Access]                            // The name... the access code (alighting and boarding)
    Opts.Field.[Stop Init Time]                         // The name... the stop-specific initial boarding penalty time
    Opts.Field.[Stop Xfer Time]                         // The name... the stop-specific transfer boarding penalty time
    Opts.Field.[Stop Dwell On Time]                     // The name... stop-specific dwell-on time
    Opts.Field.[Stop Dwell Off Time]                    // The name... stop-specific dwell-off time
    Opts.Field.[Route Fare Type]                        // The name of the field in the **route table** that contains the fare types
    Opts.Field.[Route Fare]                             // The name... flat base fares
    Opts.Field.[Route Xfer Fare]                        // The name... flat transfer fares
    Opts.Field.[Route Fare Core]                        // The name... zonal fare matrix core position (0-based)
    Opts.Field.[Route Fare Weight]                      // The name... fare weights
    Opts.Field.[Route Imp Weight]                       // The name... link impedance/time weights
    Opts.Field.[Route Init Weight]                      // The name... initial boarding penalty time weights
    Opts.Field.[Route Xfer Weight]                      // The name... transfer boarding penalty time weight
    Opts.Field.[Route IWait Weight]                     // The name... initial-waiting-time weights
    Opts.Field.[Route XWait Weight]                     // The name... transfer-waiting-time weights
    Opts.Field.[Route Dwell Weight]                     // The name... dwelling-time weights
    Opts.Field.[Route Dwell On Time]                    // The name... dwelling-on times
    Opts.Field.[Route Dwell Off Time]                   // The name... dwelling-off times
    Opts.Field.[Route Headway]                          // The name... route headwayss
    Opts.Field.[Route Init Time]                        // The name... initial boarding penalty times
    Opts.Field.[Route Xfer Time]                        // The name... transfer boarding penalty times
    Opts.Field.[Route Max IWait]                        // The name... maximum initial waiting times
    Opts.Field.[Route Min IWait]                        // The name... minimum initial waiting times
    Opts.Field.[Route Max XWait]                        // The name... maximum transfer waiting timess
    Opts.Field.[Route Min XWait]                        // The name... minimum transfer waiting times
    Opts.Field.[Route Layover Time]                     // The name... layover times
    Opts.Field.[Mode Fare Type]                         // The name of the field in the **mode table** that contains the fare types
    Opts.Field.[Mode Fare]                              // The name... flat base fares
    Opts.Field.[Mode Xfer Fare]                         // The name... flat transfer fares
    Opts.Field.[Mode Fare Core]                         // The name... zonal fare matrix core names
    Opts.Field.[Mode Fare Weight]                       // The name... fare weights
    Opts.Field.[Mode Imp Weight]                        // The name... link impedance/time weights
    Opts.Field.[Mode Init Weight]                       // The name... initial boarding penalty time weights
    Opts.Field.[Mode Xfer Weight]                       // The name... flat transfer fares
    Opts.Field.[Mode IWait Weight]                      // The name... initial-waiting-time weights
    Opts.Field.[Mode XWait Weight]                      // The name... transfer-waiting-time weights
    Opts.Field.[Mode Dwell Weight]                      // The name... dwelling-time weights
    Opts.Field.[Mode Threshold]                         // The name... threshold values for combining paths
    Opts.Field.[Mode Headway]                           // The name... headways
    Opts.Field.[Mode Init Time]                         // The name... initial boarding penalty times
    Opts.Field.[Mode Xfer Time]                         // The name... transfer boarding penalty times
    Opts.Field.[Mode Max IWait]                         // The name... maximum initial waiting times
    Opts.Field.[Mode Min IWait]                         // The name... minimum initial waiting times
    Opts.Field.[Mode Max XWait]                         // The name... maximum transfer waiting times
    Opts.Field.[Mode Min XWait]                         // The name... minimum transfer waiting times
    Opts.Field.[Mode Dwell On Time]                     // The name... dwelling-on times
    Opts.Field.[Mode Dwell Off Time]                    // The name... dwelling-off times
    Opts.Field.[Mode Layover Time]                      // The name... layover times
    Opts.Field.[Mode Max PACC]                          // The name... model maximum parking-to-station walk times (access P&R)
    Opts.Field.[Mode Max PEGR]                          // The name... model maximum station-to-parking walk times (egress P&R)
    Opts.Field.[Mode Max Access]                        // The name... maximum walk access times
    Opts.Field.[Mode Max Egress]                        // The name... maximum walk egress times
    Opts.Field.[Mode Max Transfer]                      // The name... maximum walk transfer times
    Opts.Field.[Mode Max Imp]                           // The name... maximum path impedances/times
    Opts.Field.[Mode Used]                              // The name... mode usage flags (1 to use mode, 0 otherwise)
    Opts.Field.[Mode Impedance]                         // The name... link impedance/times
    Opts.Field.[Mode Speed]                             // The name... speeds
    Opts.Field.[Mode Access]                            // The name... use-as-access flags (1 to use, 0 not to use)
    Opts.Field.[Mode Egress]                            // The name... use-as-egress flags (1 to use, 0 not to use)
    Opts.Field.[Inter-Mode Xfer From]                   // The name of the field in the **mode-to-mode cost table** that contains the From-Mode IDs
    Opts.Field.[Inter-Mode Xfer To]                     // The name... To-Mode IDs
    Opts.Field.[Inter-Mode Xfer Stop]                   // The name... transfer stop (At Stop) IDs
    Opts.Field.[Inter-Mode Xfer Proh]                   // The name... transfer prohibitions
    Opts.Field.[Inter-Mode Xfer Time]                   // The name... transfer penalty times
    Opts.Field.[Inter-Mode Xfer Fare]                   // The name... transfer fares
    Opts.Field.[Inter-Mode Xfer Wait]                   // The name... transfer wait times


    // ---------------------------------------------
    //                  FLAGS   (All strings)
    // ---------------------------------------------

    Opts.Flag.[Use All Walk Path] = "No"                // Boolean(String)          // "Yes" to allow all-walking paths (i.e. paths with no transit line), "No" otherwise
    Opts.Flag.[Use Park and Ride] = "Yes"               // Boolean(String)          // "Yes" to use park and ride access option, "No" otherwise
    Opts.Flag.[Use Egress Park and Ride] = ""           // Boolean(String)          // "Yes" to use park and ride egress option, "No" otherwise
    Opts.Flag.[Use P&R Walk Access] = ""                // Boolean(String)          // "Yes" to allow walk access when using park and ride, "No" otherwise
    Opts.Flag.[Use P&R Walk Egress] = ""                // Boolean(String)          // "Yes" to allow walk egress when using park and ride, "No" otherwise
    Opts.Flag.[Use Parking Capacity] = ""               // Boolean(String)          // "Yes" to use parking node capacity calculation, "No" otherwise
    Opts.Flag.[Use Mode] = "Yes"                        // Boolean(String)          // "Yes" to use mode tables, "No" otherwise
    Opts.Flag.[M2M Fare Method]  = 2                    // String(Number)           // Mode-to-mode transfer fare method: "Add" or "Replace"
    Opts.Flag.[Fare System] = 1                         // String(Number)           // Fare system code: "Flat", "Zonal" ,"Mixed", or "Stop"
    Opts.Flag.[Use Mode Cost]  = "Yes"                  // Boolean(String)          // "Yes" to use mode to mode transfer table, "No" otherwise; option enabled only if Use Mode above is "Yes"
    Opts.Flag.[Combine By Mode] = "Yes"                 // Boolean(String)          // "Yes" to combine routes of the same mode into a path, "No" to combine different modes
    Opts.Flag.[Fare By Mode] = "No"                     // Boolean(String)          // "Yes" to charge fare when mode changes along a path, "No" to charge fare when route changes

EndMacro
// Replica: C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\
// Original: C:\\Users\\asksy\\Desktop\\Debashish\\Workspace_MN\\

// Geneartes RouteSystem from tour table
Macro "First"
    // path variables
    path = "C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\"
    net_file = "Mandl_Net_WS_1.net"
    dbd_file = "Mandl_Links_WS_1.dbd"
    tour_file = "Routes_TF_WS_1.bin" // use the bin file

    line_layer = "Mandl Lines_WS_1"
    // Mandl_Net_WS_net|Mandl Nodes_WS_1
    // ---------------

    Opts = null
    Opts.Input.Network = path + net_file
    Opts.Input.[Link Set] = { path + dbd_file + "|" + line_layer, "Okay"}
    Opts.Input.[Tour Table] = { path + tour_file }
    // --- Table mappings
    Opts.Global.[Cost Field] = 1
    Opts.Global.[Route ID Field] = 1
    Opts.Global.[Node ID Field] = 2
    Opts.Global.[Include Stop] = 1
    Opts.Global.[Stop Flag Field] = 3
    Opts.Global.[User ID Field] = 4
    // -------------------
    Opts.Output.[Output Routes] = path + "outA\\test.rts"

    // Process --------------
    RunMacro("TCB Init")
    str = RunMacro("TCB Run Operation", "Create RS From Table", Opts)
EndMacro

/*
Macro "another macro"
    path = "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\"
    Opts = null
    Opts.Input.Network = path + "Mandl_network.net"
    Opts.Input.[Link Set] = { path + "Mandl_links_C2.dbd|Mandl Lines_C1", "Okay"}
    Opts.Input.[Tour Table] = { path + "Routes_table_new2.bin" }
    Opts.Global.[Cost Field] = 1
    Opts.Global.[Route ID Field] = 1
    Opts.Global.[Node ID Field] = 2
    Opts.Global.[Include Stop] = 1
    Opts.Global.[Stop Flag Field] = 3
    Opts.Global.[User ID Field] = 4
    Opts.Output.[Output Routes] = path + "out\\test.rts"
    RunMacro("TCB Init")
    str = RunMacro("TCB Run Operation", "Create RS From Table", Opts)
endMacro */


// Generates TransitNetwork for given Route(.rts) file
Macro "GenerateTransitNetwork"
    // Path declaration
    path = "C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\" // Don't forget \\ at end. Also \\ for paths
    route_file = "out\\test.rts" // .rts
    // map_file = "Mandl_Map_WS_R1.map" // .map
    map_file = "Mandl_with_routes.map" // .map
    // layers
    routes_layer = "Vehicle Routes"
    stops_layer = "Stops"

    Opts = null
    Opts.Input.[Transit RS] = path + route_file
    Opts.Input.[RS Set] = {path + route_file + "|" + routes_layer, routes_layer}
    Opts.Input.[Stop Set] = {path + map_file + "|" + stops_layer, stops_layer}

    // --------------------- COPY-PASTE section --------------------------------------------------
    Opts.Field.[Link Fields] = {"Distance", "Dir", "Length", "Cost", "[Bus Time]", "[Auto Time]", "[Bus Speed]", "[Auto Speed]"}
    Opts.Field.[Route Fields] = {"Route_Number", "Time", "Distance"}
    Opts.Field.[Stop Fields] = {"Pass_Count", "Milepost", "STOP_ID", "UserID", "Route_Number", "Node_ID", "Stop_Flag", "Stop_Name", "Cap"}
    Opts.Field.[Node Fields] = {"Elevation", "[ID:1]", "check"}
    Opts.Field.[Tag Method] = 1
    Opts.Field.[Tag Field] = "UserID"
    Opts.Global.[Network Label] = "Based on 'Vehicle Routes' (Thu Mar 05 15:50:54 2020)"
    Opts.Global.[Network Options].Walk = "No"
    Opts.Global.[Network Options].[Link Attributes] = {{"Distance", {"[Mandl Lines_WS_1].Distance", "[Mandl Lines_WS_1].Distance"}, "SUMFRAC"}, {"Dir", {"[Mandl Lines_WS_1].Dir", "[Mandl Lines_WS_1].Dir"}, "SUMFRAC"}, {"Length", {"[Mandl Lines_WS_1].Length", "[Mandl Lines_WS_1].Length"}, "SUMFRAC"}, {"Cost", {"[Mandl Lines_WS_1].Cost", "[Mandl Lines_WS_1].Cost"}, "SUMFRAC"}, {"[Bus Time]", {"[Mandl Lines_WS_1].[Bus Time]", "[Mandl Lines_WS_1].[Bus Time]"}, "SUMFRAC"}, {"[Auto Time]", {"[Mandl Lines_WS_1].[Auto Time]", "[Mandl Lines_WS_1].[Auto Time]"}, "SUMFRAC"}, {"[Bus Speed]", {"[Mandl Lines_WS_1].[Bus Speed]", "[Mandl Lines_WS_1].[Bus Speed]"}, "SUMFRAC"}, {"[Auto Speed]", {"[Mandl Lines_WS_1].[Auto Speed]", "[Mandl Lines_WS_1].[Auto Speed]"}, "SUMFRAC"}}
    Opts.Global.[Network Options].[Route Attributes].Route_Number = {"[Vehicle Routes].Route_Number"}
    Opts.Global.[Network Options].[Route Attributes].Time = {"[Vehicle Routes].Time"}
    Opts.Global.[Network Options].[Route Attributes].Distance = {"[Vehicle Routes].Distance"}
    Opts.Global.[Network Options].[Stop Attributes].Pass_Count = {"Stops.Pass_Count"}
    Opts.Global.[Network Options].[Stop Attributes].Milepost = {"Stops.Milepost"}
    Opts.Global.[Network Options].[Stop Attributes].STOP_ID = {"Stops.STOP_ID"}
    Opts.Global.[Network Options].[Stop Attributes].UserID = {"Stops.UserID"}
    Opts.Global.[Network Options].[Stop Attributes].Route_Number = {"Stops.Route_Number"}
    Opts.Global.[Network Options].[Stop Attributes].Node_ID = {"Stops.Node_ID"}
    Opts.Global.[Network Options].[Stop Attributes].Stop_Flag = {"Stops.Stop_Flag"}
    Opts.Global.[Network Options].[Stop Attributes].Stop_Name = {"Stops.Stop_Name"}
    Opts.Global.[Network Options].[Stop Attributes].Cap = {"Stops.Cap"}
    Opts.Global.[Network Options].[Street Node Attributes] = {{"Elevation", {"Endpoints_WS_1.Elevation"}}, {"[ID:1]", {"Endpoints_WS_1.[ID:1]"}}, {"check", {"Endpoints_WS_1.check"}}}
    Opts.Global.[Network Options].TagField = "UserID"
    Opts.Global.[Network Options].[Merge Stops] = {"Stops.ID", "Stops.UserID"}
    // ------------------------------------------------------------------------------------
    Opts.Output.[Network File] = path + "outB\\TransitNetwork.tnw" // Relative output path

    // Process -----------------------------
    RunMacro("TCB Init")
    ret_value = RunMacro("TCB Run Operation", "Build Transit Network", Opts, &Ret)
EndMacro

Macro "TableEdit"
    path = "C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\" // Don't forget \\ at end. Also \\ for paths
    // path = "C:\\Users\\asksy\\Desktop\\Debashish\\WCTRS\\Links\\"

    table_view = OpenTable("Trial Harsh", "ffb", { path + "out//testR.bin", null })
    excel_view = OpenTable("ExcelTable", "EXCELX", {path + "src//Book1.xlsx", "focusedmore" }) // Read excel file 

    AddIDField(table_view,,,,"Mode",,) // Adds a field named this
    AddIDField(table_view,,,,"Headway",,) // Adds a field named this
    AddIDField(table_view,,,,"Fare",,) // Adds a field named this
    AddIDField(table_view,,,,"XFare",,) // Adds a field named this
    AddIDField(table_view,,,,"Capacity",,) // Adds a field named this

    // m_selected = SelectByQuery("sel2", "several", query,) // Forms selection set
    
    excel_fields_array = GetFields(excel_view, "All")
    ShowArray(excel_fields_array)

    num_rows = GetRecordCount(table_view, null)
    num_rows_m = GetRecordCount(excel_view, null)

    // result = OpenFile(path + "listfile.txt", "r") // Open the file to read

    // The loop
    for i = 1 to num_rows do // num_rowsSetView(table_view)
        SetView(excel_view)
        mquery = "select * where [Route_Number] > 0"
        m_selected = SelectByQuery("sel", "several", mquery,) // Forms selection set
        excel_record = LocateRecord(excel_view + "|sel", "Route_Number", {i}, null) 
        newval = GetRecordValues(excel_view, excel_record, {"Mode", "Headway", "Fare", "Xfare", "Capacity"}) // Replace 3rd param with required column 

        SetView(table_view)
        nquery = "select * where [Route_Number] > 0"
        n_selected = SelectByQuery("sel", "several", nquery,) // Forms selection set
        tablerecord = LocateRecord(table_view + "|sel", "Route_Number", {i}, null)
        SetRecordValues(table_view, tablerecord, {
            {"Mode", newval[1][2]},
            {"Headway", newval[2][2]},
            {"Fare", newval[3][2]},
            {"Xfare", newval[4][2]},
            {"Capacity", newval[5][2]}
        })
        end

    // AddIDField(table_view,,,,"Extra",,)
    fields_array = GetFields(table_view, "All")
    
   
       // SetRowOrder(table_view, {{"Route_Number", "Ascending"}})
    // 
    // first_imp = GetFirstRecord(table_view + "|sel", null) // Using "sel" query
    // fields_array = GetFields(table_view, "All")
    // excel_array = GetFields(excel_view, "All")
    
    // SetView(table_view)
    // output_line = LocateRecord(table_view, "Route_Number", {1},)
    // GetRecordsInIndex()
    // GetNextRecords
    // values_array = GetRecordValues(table_view,, {"Route_Number"}) // Gets IDs
EndMacro

Macro "Transit Assignment"

    Opts = null
    Opts.Input.[Transit RS] = "Vehicle Routes"
    Opts.Input.Network = "C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\Transit_net_WS_2.tnw"
    Opts.Input.[OD Matrix Currencies] = {{"C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\OD_Matrix_WS_1.mtx", "Matrix 1", "Endpoints:2 (all)", "Endpoints:2 (all)"}}
    Opts.Global.[Global Alpha] = 0.15
    Opts.Global.[Global Beta] = 4
    Opts.Global.[Global Capacity] = 5000
    Opts.Global.[Global Walk Alpha] = 0
    Opts.Global.[Global Walk Beta] = 1
    Opts.Global.[Global Walk Capacity] = 8000
    Opts.Global.[Load Method] = "PFE"
    Opts.Global.[OD Layer Type] = "Node"
    Opts.Global.[Force Threads] = 8
    Opts.Global.Iterations = 20
    Opts.Global.Convergence = 0.0001
    Opts.Global.[Skim Variables] = {"Generalized Cost"}
    Opts.Global.[OD Core Names] = {"Matrix 1"}
    Opts.Global.[OD Class Names] = {"Class 1"}
    Opts.Flag.[Do Aggre Report] = 1
    Opts.Flag.[Do Theme] = 0
    Opts.Flag.[Load Drive Trips] = 1
    Opts.Flag.[Load Walk Trips] = 1
    Opts.Local.[Do OnOff Report] = 1
    Opts.Local.[Do Skimming] = 1
    Opts.Local.[Do Modal Skimming] = 0
    Opts.Local.[Report Access Stops] = 0
    Opts.Local.[Report Egress Stops] = 0

    RunMacro("TCB Run Procedure", "Transit Assignment", Opts)

EndMacro
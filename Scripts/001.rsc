// Replica: C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\
// Original: C:\\Users\\asksy\\Desktop\\Debashish\\Workspace_MN\\

// Geneartes RouteSystem from tour table
Macro "CreateRouteTable"
    // path variables
    path = "C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\"
    net_file = "Mandl_Net_WS_1.net"
    dbd_file = "Mandl_Links_WS_1.dbd"
    tour_file = "Routes_TF_WS_1.bin" // use the bin file

    line_layer = "Mandl Lines_WS_1"
    // ---------------

    Opts = null
    Opts.Input.Network = path + net_file
    Opts.Input.[Link Set] = { path + dbd_file + "|" + line_layer, "Okay"}
    Opts.Input.[Tour Table] = { path + tour_file }
    // --- Table mappings
    Opts.Global.[Cost Field] = 1
    Opts.Global.[Route ID Field] = 1
    Opts.Global.[Node ID Field] = 2
    Opts.Global.[Include Stop] = 1
    Opts.Global.[Stop Flag Field] = 3
    Opts.Global.[User ID Field] = 4
    // -------------------
    Opts.Output.[Output Routes] = path + "out\\test.rts"

    // Process --------------
    RunMacro("TCB Init")
    str = RunMacro("TCB Run Operation", "Create RS From Table", Opts)
EndMacro

// Generates TransitNetwork for given Route(.rts) file
Macro "BuildTransitNetwork"
    // Path declaration
    path = "C:\\Users\\ASK\\Desktop\\Debashish\\transcad-plugins\\Samples\\Workspace_MN\\" // Don't forget \\ at end. Also \\ for paths
    route_file = "out\\test.rts" // .rts
    map_file = "Mandl_Map_WS_R1.map" // .map
    // layers
    routes_layer = "Vehicle Routes"
    stops_layer = "Stops"

    Opts = null
    Opts.Input.[Transit RS] = path + route_file
    Opts.Input.[RS Set] = {path + route_file + "|" + routes_layer, routes_layer}
    Opts.Input.[Stop Set] = {path + map_file + "|" + stops_layer, stops_layer}

    // --------------------- COPY-PASTE section --------------------------------------------------
    Opts.Field.[Link Fields] = {"Distance", "Dir", "Length", "Cost", "[Bus Time]", "[Auto Time]", "[Bus Speed]", "[Auto Speed]"}
    Opts.Field.[Route Fields] = {"Route_Number", "Time", "Distance"}
    Opts.Field.[Stop Fields] = {"Pass_Count", "Milepost", "STOP_ID", "UserID", "Route_Number", "Node_ID", "Stop_Flag", "Stop_Name", "Cap"}
    Opts.Field.[Node Fields] = {"Elevation", "[ID:1]", "check"}
    Opts.Field.[Tag Method] = 1
    Opts.Field.[Tag Field] = "UserID"
    Opts.Global.[Network Label] = "Based on 'Vehicle Routes' (Thu Mar 05 15:50:54 2020)"
    Opts.Global.[Network Options].Walk = "No"
    Opts.Global.[Network Options].[Link Attributes] = {{"Distance", {"[Mandl Lines_WS_1].Distance", "[Mandl Lines_WS_1].Distance"}, "SUMFRAC"}, {"Dir", {"[Mandl Lines_WS_1].Dir", "[Mandl Lines_WS_1].Dir"}, "SUMFRAC"}, {"Length", {"[Mandl Lines_WS_1].Length", "[Mandl Lines_WS_1].Length"}, "SUMFRAC"}, {"Cost", {"[Mandl Lines_WS_1].Cost", "[Mandl Lines_WS_1].Cost"}, "SUMFRAC"}, {"[Bus Time]", {"[Mandl Lines_WS_1].[Bus Time]", "[Mandl Lines_WS_1].[Bus Time]"}, "SUMFRAC"}, {"[Auto Time]", {"[Mandl Lines_WS_1].[Auto Time]", "[Mandl Lines_WS_1].[Auto Time]"}, "SUMFRAC"}, {"[Bus Speed]", {"[Mandl Lines_WS_1].[Bus Speed]", "[Mandl Lines_WS_1].[Bus Speed]"}, "SUMFRAC"}, {"[Auto Speed]", {"[Mandl Lines_WS_1].[Auto Speed]", "[Mandl Lines_WS_1].[Auto Speed]"}, "SUMFRAC"}}
    Opts.Global.[Network Options].[Route Attributes].Route_Number = {"[Vehicle Routes].Route_Number"}
    Opts.Global.[Network Options].[Route Attributes].Time = {"[Vehicle Routes].Time"}
    Opts.Global.[Network Options].[Route Attributes].Distance = {"[Vehicle Routes].Distance"}
    Opts.Global.[Network Options].[Stop Attributes].Pass_Count = {"Stops.Pass_Count"}
    Opts.Global.[Network Options].[Stop Attributes].Milepost = {"Stops.Milepost"}
    Opts.Global.[Network Options].[Stop Attributes].STOP_ID = {"Stops.STOP_ID"}
    Opts.Global.[Network Options].[Stop Attributes].UserID = {"Stops.UserID"}
    Opts.Global.[Network Options].[Stop Attributes].Route_Number = {"Stops.Route_Number"}
    Opts.Global.[Network Options].[Stop Attributes].Node_ID = {"Stops.Node_ID"}
    Opts.Global.[Network Options].[Stop Attributes].Stop_Flag = {"Stops.Stop_Flag"}
    Opts.Global.[Network Options].[Stop Attributes].Stop_Name = {"Stops.Stop_Name"}
    Opts.Global.[Network Options].[Stop Attributes].Cap = {"Stops.Cap"}
    Opts.Global.[Network Options].[Street Node Attributes] = {{"Elevation", {"Endpoints_WS_1.Elevation"}}, {"[ID:1]", {"Endpoints_WS_1.[ID:1]"}}, {"check", {"Endpoints_WS_1.check"}}}
    Opts.Global.[Network Options].TagField = "UserID"
    Opts.Global.[Network Options].[Merge Stops] = {"Stops.ID", "Stops.UserID"}
    // ------------------------------------------------------------------------------------
    Opts.Output.[Network File] = path + "out\\TransitNetwork.tnw" // Relative output path

    // Process -----------------------------
    RunMacro("TCB Init")
    ret_value = RunMacro("TCB Run Operation", "Build Transit Network", Opts, &Ret)
EndMacro

/*
Macro "another macro"
    path = "C:\\Users\\asksy\\Desktop\\Debashish\\my_mandl_network\\"
    Opts = null
    Opts.Input.Network = path + "Mandl_network.net"
    Opts.Input.[Link Set] = { path + "Mandl_links_C2.dbd|Mandl Lines_C1", "Okay"}
    Opts.Input.[Tour Table] = { path + "Routes_table_new2.bin" }
    Opts.Global.[Cost Field] = 1
    Opts.Global.[Route ID Field] = 1
    Opts.Global.[Node ID Field] = 2
    Opts.Global.[Include Stop] = 1
    Opts.Global.[Stop Flag Field] = 3
    Opts.Global.[User ID Field] = 4
    Opts.Output.[Output Routes] = path + "out\\test.rts"
    RunMacro("TCB Init")
    str = RunMacro("TCB Run Operation", "Create RS From Table", Opts)
endMacro */
## Route System files

| Type         | Extension | Location     | Table Description                                                       |
| ------------ | --------- | ------------ | ----------------------------------------------------------------------- |
| Access       | .rts      | singleton    |                                                                         |
| Route Table  | .bin      | ${name}R.bin | Route_ID - Route_Name - Route_Number - Time - Distance                  |
| Stop Table   | .bin      | ${name}S.bin | Stop_ID - UserID - Route_Number - Node_ID - Stop_Flag - Stop_Name - Cap |
| Link Table   | .bin      | ${name}c.bin | Route_ID - Link_ID                                                      |
| Route - Link | .bin      | ${name}L.bin | Route_ID - Link_ID - Direction                                          |
